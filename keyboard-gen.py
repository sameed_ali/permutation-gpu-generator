KeyboardRow1 = ["`", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", ""]
KeyboardRow2 = ["", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", ""]
KeyboardRow3 = ["", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "", "", ""]
KeyboardRow4 = ["", "z", "x", "c", "v", "b", "n", "m", ",", ".", "/", "", "", ""]
KeyboardRow1S = ["~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", ""]
KeyboardRow2S = ["", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "{", "}", "|"]
KeyboardRow3S = ["", "A", "S", "D", "F", "G", "H", "J", "K", "L", ":", "", "", ""]
KeyboardRow4S = ["", "Z", "X", "C", "V", "B", "N", "M", "<", ">", "?", "", "", ""]

Rows = [KeyboardRow1, KeyboardRow2, KeyboardRow3, KeyboardRow4, KeyboardRow1S, KeyboardRow2S, KeyboardRow3S, KeyboardRow4S]


def isAdjacentKey(key0, key1):
    iCord = None
    jCord = None
    for i in range(0, len(Rows)):
        for j in range(0, len(Rows[i])):
            if key0 == Rows[i][j]:
                iCord = (i, j)
            if key1 == Rows[i][j]:
                jCord = (i, j)
            # when both are known compute adjacency and return
            if (iCord is not None) and (jCord is not None):
                return isAdjacentCord(iCord, jCord)
    raise Exception("Key not found")


def isAdjacentCord(Coord1, Coord2):
    if abs((Coord1[0] - Coord2[0]) > 1) or (abs(Coord1[1] - Coord2[1]) > 1):
        return False
    else:
        return True


print isAdjacentKey('q', 's')

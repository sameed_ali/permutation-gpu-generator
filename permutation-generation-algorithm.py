import pprint


def lexPerm(arr):
    """
    Generates n! lexicographical permutations
    """
    if len(arr) is 0:
        return []
    elif len(arr) is 1:
        return [arr]
    else:
        res = []
        for i in arr:
            tmpArr = list(arr)
            tmpArr.remove(i)
            res.extend([[i] + x for x in lexPerm(tmpArr)])
        return res


def factorial(i):
    "Returns the i!"
    res = 1
    iter = 1
    while (iter <= i):
        res *= iter
        iter += 1
    return res


def factoroidPerm(i, seq):
    """
    Uses the factorial decomposition to generate the ith permutation
    """
    if i is 0:
        return seq
    perm = list(seq)
    for k in range(0, len(seq)):
        factVal = factorial((len(seq) - 1))
        div = i / factVal
        mod = i % factVal
        selected = seq[div]
        perm[k] = selected
        seq = filter(lambda x: x != selected, seq)
        i = mod
    return perm


def main():
    seq = range(0, 5)
    maxRange = factorial(len(seq))
    bruteforce = lexPerm(seq)
    # pprint.pprint(bruteforce)
    for i in range(0, maxRange):
        # print "num:", i
        # print bruteforce[i]
        # print "==="
        assert(cmp(bruteforce[i], factoroidPerm(i, seq)) is 0)
        # print factoroidPerm(i, seq)
        # print (float(i) / maxRange) * 100, " done"


if __name__ == "__main__":
    main()
